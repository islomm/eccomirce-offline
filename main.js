const product = [
  {
    id: 1,
    image: "./img/img1.png",
    title: "Bag",
    price: 110,
  },
  {
    id: 1,
    image: "./img/img2.png",
    title: "Cap",
    price: 90,
  },
  {
    id: 1,
    image: "./img/img3.png",
    title: "Pencil",
    price: 80,
  },
  {
    id: 1,
    image: "./img/img4.png",
    title: "T-shirt",
    price: 50,
  },
];

const categories = [...new Set(product.map((item) => item))];
let i = 0;
let j = 0;
document.querySelector("#root").innerHTML = categories
  .map((item) => {
    var { image, title, price } = item;
    return `<div class="box">
            <div className="image-box">
                <img src=${image} alt="" />
            </div>
            <div className="bottom">
                <p>${title}</p>
                <h2>Price ${price}.00</h2>
                <button onClick="addToCart(${i++})">Add to cart</button>
                <button onClick="addToLiked(${j++})" class="like-btn">❤️</button>
            </div>
        </div>`;
  })
  .join("");

let cart = [];
let like = [];
let admin = [];
const clear = document.querySelector(".clear");
clear.addEventListener("click", () => {
  clearAll();
  localStorage.setItem("admin", JSON.stringify(admin));
});
function clearAll() {
  admin.splice(0, admin.length);
  cart.splice(0, cart.length);
  displayCart();
  displayProducts();
}
//--------------------
function addToLiked(id) {
  like.push(categories[id]);
  displayLiked();
  localStorage.setItem("like", JSON.stringify(like));
}
function delLiked(id) {
  like.splice(id, 1);
  displayLiked();
  localStorage.setItem("like", JSON.stringify(like));
}
function delProduct(id) {
  admin.splice(id, 1);
  cart.splice(id, 1);
  displayCart();
  displayProducts();
  localStorage.setItem("admin", JSON.stringify(admin));
}
//-------------------
function addToCart(id) {
  cart.push(categories[id]);
  admin.push(categories[id]);
  displayCart();
  displayProducts();
  localStorage.setItem("admin", JSON.stringify(admin));
  localStorage.setItem("cart", JSON.stringify(cart));
}
function delElement(id) {
  cart.splice(id, 1);
  admin.splice(id, 1);
  displayCart();
  displayProducts();
  localStorage.setItem("admin", JSON.stringify(admin));
  localStorage.setItem("cart", JSON.stringify(cart));
}
console.log(admin);
function totalRes() {
  let result = 0;
  cart.map((item) => (result += item.price));
  localStorage.setItem("cart", JSON.stringify(cart));
  return result;
}
function displayCart() {
  let j = 0;
  document.getElementById("count").textContent = cart.length;
  document.getElementById("total").textContent = totalRes();
  if (cart.length == 0) {
    document.querySelector(".cartItem").textContent =
      "Savatchada mahsulotlar yoq";
  } else {
    document.querySelector(".cartItem").innerHTML = cart
      .map((item) => {
        console.log(item);
        var { image, title, price } = item;
        return `<div class="cart-item">
                        <div class="row-img">
                            <img class="rowing" src=${image} alt="" />
                        </div>
                        <p style="font-size:12px">${title}</p>
                        <h2 style="font-size: 15px">${price}$</h2>
                        <i class="fa fa-trash" onclick='delElement(${j++})'></i>
                    </div>`;
      })
      .join("");
  }
}

function displayLiked() {
  let k = 0;
  document.getElementById("countLike").textContent = like.length;
  if (like.length == 0) {
    document.querySelector(".cartLiked").textContent =
      "Savatchada yoqtirgan mahsulotlar yoq";
  } else {
    document.querySelector(".cartLiked").innerHTML = like
      .map((item) => {
        console.log(item);
        var { image, title, price } = item;
        return `<div class="cart-item">
                          <div class="row-img">
                              <img class="rowing" src=${image} alt="" />
                          </div>
                          <p style="font-size:12px">${title}</p>
                          <h2 style="font-size: 15px">${price}$</h2>
                          <i class="fa fa-trash" onclick='delLiked(${k++})'></i>
                      </div>`;
      })
      .join("");
  }
}
document.addEventListener("DOMContentLoaded", () => {
  cart = JSON.parse(localStorage.getItem("cart"));
  if (cart) {
    displayCart();
  } else {
    cart = [];
  }
  like = JSON.parse(localStorage.getItem("like"));
  if (like) {
    displayLiked();
  } else {
    like = [];
  }
  admin = JSON.parse(localStorage.getItem("admin"));
  if (admin) {
    displayProducts();
  } else {
    admin = [];
  }
});
